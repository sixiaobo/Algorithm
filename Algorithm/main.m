//
//  main.m
//  Algorithm
//
//  Created by 司小波 on 16/8/2.
//  Copyright © 2016年 司小波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
