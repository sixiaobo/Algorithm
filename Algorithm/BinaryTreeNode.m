//
//  BinaryTreeNode.m
//  PicSearch
//
//  Created by 司小波 on 2017/8/1.
//  Copyright © 2017年 com.sixiaobo. All rights reserved.
//

#import "BinaryTreeNode.h"

typedef struct binary Bnode;
struct binary {
    int value;
    Bnode *left;
    Bnode *right;
};


/**
 * 创建二叉排序树, 数组，节点个数，根
 */
Bnode creatTreeWithValues(int *values, int count, Bnode *node, int i) {
    int value = values[i];
    if (node == NULL) {
        Bnode mNode = {value, NULL, NULL};
        node = &mNode;
    }
    int index = i+1;
    if (index > count - 1) {
        return *node;
    }
    if (values[index] <= node->value) {
        Bnode mnode = creatTreeWithValues(values, count, node, index);
        node->left = &mnode;
    } else {
        Bnode mnode = creatTreeWithValues(values, count, node, index);
        node->right = &mnode;
    }
    return *node;
}


#pragma mark- 指针函数回调
void act(Bnode node) {
    NSLog(@"二叉树节点: %d", node.value);
}

typedef void (*Pfunc)(Bnode);



/*
 * 中序遍历二叉树
 **/

void inOrderTree(Bnode *root, Pfunc func) {
    if (root != NULL) {
        
        NSLog(@"%d  ", root->left->value);
        
        inOrderTree(root->left, func);
        func(*root);
        inOrderTree(root->right, func);
    }
}


/**
 * 层次遍历二叉树
 */
void levelTraverseTree(Bnode root, Pfunc func) {
    Bnode nodes[100];
    nodes[0] = root;
    int count = 1;
    while (count > 0) {
        Bnode node = nodes[0];
        func(node);
        if (count > 1) {
            for (int i = 0; i < count - 1; i++) {
                nodes[i] = nodes[i+1];
            }
        }
        if (node.left && node.right) {
            nodes[0] = *node.left;
            nodes[1] = *node.right;
            count = 2;
        } else if (node.left && !node.right) {
            nodes[0] = *node.left;
            count = 1;
        } else if (node.right && !node.left) {
            nodes[0] = *node.right;
            count = 1;
        } else {
            count--;
        }
    }
}



@implementation BinaryTreeNode


/**
 * 二叉排序树
 */
+ (BinaryTreeNode *)nodeWithValue:(NSInteger)value root:(BinaryTreeNode *)root {
    if (!root) {
        root = [BinaryTreeNode new];
        root.value = value;
    } else if (value <= root.value) {
        root.left = [self nodeWithValue:value root:root.left];
    } else {
        root.right = [self nodeWithValue:value root:root.right];
    }
    return root;
}



+ (BinaryTreeNode *)createTree:(NSArray *)values {
    BinaryTreeNode *root = nil;
    for (NSInteger i = 0; i < values.count; i++) {
        NSNumber *num = values[i];
        root = [self nodeWithValue:num.integerValue root:root];
    }
    return root;
}




void nodeFunc(BinaryTreeNode *node) {
    NSLog(@":  %lu", node.value);
}

typedef void (*NFP)(BinaryTreeNode *);


/**
 * 中顺序遍历
 */
+ (void)inOrder:(BinaryTreeNode *)root nfp:(NFP)nfp {
    if (root) {
        [self inOrder:root.left nfp:nfp];
        nfp(root);
        [self inOrder:root.right nfp:nfp];
    }
}



/**
 * 层遍历,广度优先遍历
 */
+ (void)levelTree:(BinaryTreeNode *)root nfp:(NFP)nfp {
    if (!root) {
        return;
    }
    NSMutableArray *queue = [NSMutableArray new];
    [queue addObject:root];
    
    while (queue.count > 0) {
        BinaryTreeNode *node = queue.firstObject;
        nfp(node);
        [queue removeObjectAtIndex:0];
        if (node.left) {
            [queue addObject:node.left];
        }
        if (node.right) {
            [queue addObject:node.right];
        }
    }
}



/**
 * 某个位置的节点(按层遍历)
 */
+ (BinaryTreeNode *)noteAtIndex:(NSInteger)index root:(BinaryTreeNode *)root {
    if (!root) {
        return nil;
    }
    NSMutableArray *queue = [NSMutableArray array];
    [queue addObject:root];
    
    NSInteger i = 0;
    while (queue.count > 0) {
        BinaryTreeNode *node = queue.firstObject;
        if (i == index) {
            return node;
        }
        [queue removeObjectAtIndex:0];
        if (node.left) {
            [queue addObject:node.left];
        }
        if (node.right) {
            [queue addObject:node.right];
        }
        i++;
    }
    return nil;
}



/**
 * 深度
 */
+ (NSInteger)deepOfTree:(BinaryTreeNode *)root {
    if (!root) {
        return 0;
    }
    if (!root.left && !root.right) {
        return 1;
    }
    NSInteger leftDeep = [self deepOfTree:root.left];
    NSInteger rightDeep = [self deepOfTree:root.right];
    return MAX(leftDeep, rightDeep) + 1;
}


/**
 * 宽度
 */
+ (NSInteger)widthOfTree:(BinaryTreeNode *)root {
    if (!root) {
        return 0;
    }
    NSInteger width = 0; //用指针的话一定要记着初始化！
    [self widthOfTreeInArr:@[root] width:&width];
    return width;
}



+ (void)widthOfTreeInArr:(NSArray *)arr width:(NSInteger *)width {
    @autoreleasepool {
        NSMutableArray *muarr = [NSMutableArray array];
        for (BinaryTreeNode *node in arr) {
            if (node.left) {
                [muarr addObject:node.left];
            }
            if (node.right) {
                [muarr addObject:node.right];
            }
        }
        NSInteger count = MAX(arr.count, muarr.count);
        if (*width < count) {
            *width = count;
        }
        if (muarr.count) {
            [self widthOfTreeInArr:muarr width:width];
        }
    }
}



/**
 * 宽度算法2
 */
+ (NSInteger)widthWithTree:(BinaryTreeNode *)root {
    if (!root) {
        return 0;
    }
    NSMutableArray *queue = [NSMutableArray array];
    [queue addObject:root];
    NSInteger width = queue.count;
    NSInteger mark = 0;
    while (mark < queue.count) {
        NSInteger markCount = queue.count;
        if (markCount - mark > width) {
            width = markCount - mark;
        }
        for (NSInteger i = mark; i < markCount; i++) {
            BinaryTreeNode *node = queue[i];
            if (node.left) {
                [queue addObject:node.left];
            }
            if (node.right) {
                [queue addObject:node.right];
            }
        }
        mark = markCount;
    }
    return width;
}



/**
 * 所有节点数
 */
+ (NSInteger)countOfTree:(BinaryTreeNode *)root {
    if (!root) {
        return 0;
    }
    return [self countOfTree:root.left] + [self countOfTree:root.right] + 1;
}



/**
 * 某层中的节点数
 */
+ (NSInteger)countOfTree:(BinaryTreeNode *)root level:(NSInteger)level {
    if (!root) {
        return 0;
    }
    NSMutableArray *queue = [NSMutableArray array];
    [queue addObject:root];
    NSInteger mark = 0, num = 1; //num 是层编号
    while (mark < queue.count) {
        NSInteger count = queue.count;
        if (level == num) {
            return count - mark;
        }
        for (NSInteger i = mark; i < count; i++) {
            BinaryTreeNode *node = queue[i];
            if (node.left) {
                [queue addObject:node.left];
            }
            if (node.right) {
                [queue addObject:node.right];
            }
        }
        mark = count;
        num++;
    }
    return 0;
}

/**
 * 叶子结点数(左右子树都为空)
 */
+ (NSInteger)countOfLeafsOnTree:(BinaryTreeNode *)root {
    if (!root) {
        return 0;
    }
    NSMutableArray *queue = [NSMutableArray new];
    [queue addObject:root];

    NSInteger leafCount = 0;
    while (queue.count > 0) {
        BinaryTreeNode *node = queue.firstObject;
        [queue removeObjectAtIndex:0];
        if (node.left) {
            [queue addObject:node.left];
        }
        if (node.right) {
            [queue addObject:node.right];
        }
        if (!node.left && !node.right) {
            leafCount++;
        }
    }
    return leafCount;
}


/**
 * 二叉树的直径(两节点间最大路径)
 */
+ (NSInteger)lengthOfTree:(BinaryTreeNode *)root {
    return 0;
}



/**
 * 二叉树某节点到根节点路径
 */

+ (NSArray *)pathOfNodeToRoot:(BinaryTreeNode *)node root:(BinaryTreeNode *)root {
    NSMutableArray *path = [NSMutableArray new];
    [self isFondRoot:root node:node path:path];
    return path;
}


/**
 * 是否存在节点
 */
+ (BOOL)isFondRoot:(BinaryTreeNode *)root node:(BinaryTreeNode *)node path:(NSMutableArray *)path {
    if (!root || !node) {
        return NO;
    }
    if (root == node) {
        [path addObject:root];
        return YES;
    }
    [path addObject:root];
    BOOL isFind = [self isFondRoot:root.left node:node path:path];
    if (!isFind) {
       isFind = [self isFondRoot:root.right node:node path:path];
    }
    if (!isFind) {   //两边都未找到，推出栈
        [path removeObject:root];
    }
    return isFind;
}


/**
 * 二叉树两节点间路径
 */

+ (NSArray *)pathOfNode0:(BinaryTreeNode *)node0 node1:(BinaryTreeNode *)node1 root:(BinaryTreeNode *)root {
    NSArray *path0 = [self pathOfNodeToRoot:node0 root:root];
    NSArray *path1 = [self pathOfNodeToRoot:node1 root:root];
    
    
    //自上而下寻找，最后一个node就是最近node
    NSInteger k = 0, q = 0;
    for (NSInteger i = 0; i < path0.count; i++) {
        BinaryTreeNode *indexNode0 = path0[i];
        for (NSInteger j = 0; j < path1.count; j++) {
            BinaryTreeNode *indexNode1 = path1[j];
            if (indexNode0 == indexNode1) {
                k = i, q = j;
            }
        }
    }
    NSMutableArray *muarr = [[NSMutableArray alloc] init];
    for (NSInteger i = path0.count - 1; i > k; i--) {
        [muarr addObject:path0[i]];
    }
    NSArray *arr1 = [path1 subarrayWithRange:NSMakeRange(q, path1.count - q)];
    [muarr addObjectsFromArray:arr1];
    return muarr;
}


/**
 * 两节点间距离，即两节点间路径数减去1
 */

+ (NSInteger)distanceWithNode0:(BinaryTreeNode *)node0 node1:(BinaryTreeNode *)node1 root:(BinaryTreeNode *)root {
    NSArray *path = [self pathOfNode0:node0 node1:node1 root:root];
    return path.count - 1;
}



/**
 * 翻转二叉树
 */
+ (void)turnTree:(BinaryTreeNode *)root {
    NSMutableArray *queue = [NSMutableArray new];
    [queue addObject:root];
    
    while (queue.count > 0) {
        BinaryTreeNode *node = queue.firstObject;
        [queue removeObjectAtIndex:0];
        
        BinaryTreeNode *temp = node.left;
        node.left = node.right;
        node.right = temp;
        
        if (node.left) {
            [queue addObject:node.left];
        }
        if (node.right) {
            [queue addObject:node.right];
        }
    }
}


/**
 * 是否完全二叉树
 */
+ (BOOL)isCompleteTree:(BinaryTreeNode *)root {
    //NSMutableArray *queue = [NSMutableArray array];
    return NO;
}



+ (void)load {
//    Bnode left = {.value = 2}, right = {.value = 5};
//    Bnode root = {4, &left, &right};
    
//    int a[] = {30, 5, 6, 8, 3, 4, 9, 10, 100, 35, 65, 80};
//    Bnode root = creatTreeWithValues(a, 12, NULL, 0);
//    levelTraverseTree(root, &act);
//    inOrderTree(&root, &act);
    
    BinaryTreeNode *root = [self createTree:@[@8, @1, @3, @5, @25, @15, @10, @60, @50, @20, @100, @12]];
    [self inOrder:root nfp:&nodeFunc];
    printf("\n\n");
    [self levelTree:root nfp:&nodeFunc];
    
    BinaryTreeNode *node = [self noteAtIndex:4 root:root];
    
    NSLog(@"二叉树 deepth:  %lu   width: %lu  node: %lu  count: %lu  countLevel: %lu  leafs: %lu", [self deepOfTree:root], [self widthWithTree:root], node.value,
          [self countOfTree:root], [self countOfTree:root level:4], [self countOfLeafsOnTree:root]);
    
    
    
    NSLog(@"-------12 到根的路径 --------");
    NSArray *arr = [self pathOfNodeToRoot:[self noteAtIndex:11 root:root] root:root];  //20到根的路径
    [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        BinaryTreeNode *node = obj;
        NSLog(@"%lu", node.value);
    }];
    
    
    NSLog(@"-------12 到 100 的路径 --------");
    NSArray *path = [self pathOfNode0:[self noteAtIndex:11 root:root] node1:[self noteAtIndex:10 root:root] root:root];
    
    [path enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        BinaryTreeNode *node = obj;
        NSLog(@"%lu", node.value);
    }];
    
    
    NSLog(@"-----翻转后的-----");
    [self turnTree:root];
    [self inOrder:root nfp:&nodeFunc];
    

}



@end
