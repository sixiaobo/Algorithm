//
//  BinaryTreeNode.h
//  PicSearch
//
//  Created by 司小波 on 2017/8/1.
//  Copyright © 2017年 com.sixiaobo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BinaryTreeNode : NSObject
@property (nonatomic, assign) NSInteger value;
@property (nonatomic, strong) BinaryTreeNode *left;
@property (nonatomic, strong) BinaryTreeNode *right;


@end
