//
//  ViewController.m
//  Algorithm
//
//  Created by 司小波 on 16/8/2.
//  Copyright © 2016年 司小波. All rights reserved.
//

#import "ViewController.h"
#import "Person.h"
#import <objc/runtime.h>

@interface ViewController ()

@end

@implementation ViewController



int func(int *a) {
    printf("%d\n\n",a[1]);
    char* str = "ABCDEFGHIJKL";
    int* pInt = (int*)str;    //转换成了int类型的指针，存储的还是char类型数据的空间地址,int类型的指针移动一步,地址空间就加4，因为sizeof(int)是4
    printf("%c\n",*pInt);
    
    printf("%c\n%c\n%lu\n",*(str+1), *(char *)(pInt + 1), sizeof(int));
    return 0;
}

#pragma mark- 快速排序,left左下标，right右下标
void quckSort(int *a, int left, int right) {
    if (left > right) {
        return;
    }
    int temp = a[left], i = left, j = right;  //temp是中间数
    while (i<j) {
        while (a[j] >= temp && i<j) {
            j--;
        }
        while (a[i] <= temp && i<j) {
            i++;
        }
        if (i<j) {
            int t = a[i];
            a[i] = a[j];
            a[j] = t;
        }
    }
    a[left] = a[i];
    a[i] = temp;
    quckSort(a, left, i-1);
    quckSort(a, i+1, right);
}



#pragma mark- 冒泡
void bobSort(int *a, int count) {
    for (int i = count; i > 0; i--) {
        for (int j = 0; j < i; j++) {
            if (a[j] < a[j+1]) {
                int t = a[j];
                a[j] = a[j+1];
                a[j+1] = t;
            }
        }
    }
}


#pragma mark- 选择排序
void chooseSort(int *a, int count) {
    for (int i = 0; i < count - 1; i++) {
        int temp = a[i+1];
        int c = 0;
        for (int j = i+2; j<count; j++) {    //先选出最大的
            if (temp < a[j]) {
                temp = a[j];
                c = j;
            }
        }
        if (temp > a[i]) {  //交换
            a[c] = a[i];
            a[i] = temp;
        }
    }
}





#pragma mark- 插入排序
void insertSort(int *a, int count) {
    for (int i = 1; i < count; i++) {
        int j = i;
        int target = a[i];
        while (j > 0 && target < a[j-1]) {   //右移
            a[j] = a[j-1];
            j--;
        }
        a[j] = target;    //插入
    }
}







- (void)viewDidLoad {
    [super viewDidLoad];
    int a[] = {6, 8, 9, 7, 2, 1, 4, 3, 5, 89, 35, 67, 800, 59, 3,7,99,102,100};
    func(a);
    int count = sizeof(a)/sizeof(a[0]);
    quckSort(a, 0, count - 1);
    printf("\n");
    for (int i = 0; i< count; i++) {
        printf("%d ",a[i]);
    }
    bobSort(a, count);
    printf("\n");
    for (int i = 0; i< count; i++) {
        printf("%d ",a[i]);
    }
    insertSort(a, count);
    printf("\n");
    for (int i = 0; i< count; i++) {
        printf("%d ",a[i]);
    }
    chooseSort(a, count);
    printf("\n");
    for (int i = 0; i< count; i++) {
        printf("%d ",a[i]);
    }
    printf("\n");
    
    
    [self funcRuntimeChangeVar];
    
    
    NSValue *value = [self funcValueBlock];
    void (^strBlock)(NSString *str);
    [value getValue:&strBlock];
    strBlock(@"这是一个牛逼的value包装的block");
}



//修改变量
- (void)funcRuntimeChangeVar {
    
    Person *p = [[Person alloc] init];
    p.name = @"小明";
    unsigned int count;
    Ivar *ivar = class_copyIvarList([p class], &count);
    for (int i = 0; i < count; i++) {
        Ivar var = ivar[i];
        NSString *name = [NSString stringWithUTF8String:ivar_getName(var)];
        if ([name isEqualToString:@"_age"]) {
            object_setIvar(p, var, @(23));
        }
    }
    NSLog(@"age:%@", p.age);
}




- (NSValue *)funcValueBlock {
    void (^strBlock)(NSString *str) = ^(NSString *str) {
        NSLog(@"beautiful + %@",str);
    };
    NSValue *value = [NSValue value:&strBlock withObjCType:@encode(void(^)(NSString *))];
    return value;
}






@end
