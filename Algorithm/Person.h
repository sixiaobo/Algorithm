//
//  Person.h
//  Algorithm
//
//  Created by 司小波 on 16/8/2.
//  Copyright © 2016年 司小波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject
@property (nonatomic, assign) BOOL beautiful;
@property (nonatomic, strong) NSNumber *age;
@property (nonatomic, copy) NSString *name;


@end
